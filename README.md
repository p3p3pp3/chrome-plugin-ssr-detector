# SSR地址收集器Chrome小小小插件

简介：从含有SS://或者SSR://链接的页面提取所有地址，以空格分隔拷贝至剪贴板，方便使用SS客户端批量导入。

## 安装：

1. 下载最新版本的插件https://gitlab.com/p3p3pp3/chrome-plugin-ssr-detector/tags
2. 打开Chrome->设置->扩展程序
3. 拖放下载的chrome-plugin-ssr-detector.crx文件到扩展程序列表，完成安装。

## 用法：

1. 打开带有SS/SSR链接地址的页面，比如 http://guapi.tk (感谢站长OneGuaPi提供的资源)，点击Chrome扩展中的绿色SSR图标。
2. 将页面拉到底部，会看到一行「点击复制SSR地址表(By p3p3pp3)」，点击它，提示「已复制到剪贴板」，这时，一大波地址已经在剪贴板里面了。
3. 以ShadowsocksR为例，邮件点击托盘栏小飞机图标，选择「剪贴板批量导入SSR地址」。

所有的SSR服务器就添加完成了，是不是很方便呢。

## 有BUG？

我的Twitter *https://twitter.com/p3p3pp3* ，遇到问题可以@我，预祝翻墙愉快。